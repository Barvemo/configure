import os
import pwd

# list users to exist
users = ["admineros", "admin", "linus", "barvemo", "nsa", "cia", 'banarne', 'trazan', 'galileo', 'mozart']

for user in users:
    try:
        # check if user already exists
        pwd.getpwnam(user)
        print(f'User {user} already exist. Moving on to the next user...')
    except KeyError:
        # add user
        print(f'User {user} does not exist.')
        add_user = f'adduser {user} --disabled-password --gecos ""'
        os.system(add_user)

        # ssh: add .ssh directory, set owner:group, set permissions
        add_ssh_dir = f'mkdir /home/{user}/.ssh' 
        os.system(add_ssh_dir)

        set_owner_group = f'chown {user}:{user} /home/{user}/.ssh'
        os.system(set_owner_group)

        add_ssh_permissions = f'chmod 700 /home/{user}/.ssh'
        os.system(add_ssh_permissions)

        # sudo: add user to sudoers
        add_sudo = f'adduser {user} sudo'
        os.system(add_sudo)
