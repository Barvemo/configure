import os

# retrieve hostname name
hostname = input('Hostname: ')

# set hostname
set_hostname = f'hostnamectl set-hostname {hostname}'
os.system(set_hostname)

# add hostname to /etc/hosts just below localhost
hosts_insert = f'sed -i "/\localhost/a {hostname}" /etc/hosts'
os.system(hosts_insert)
